/*global define*/

define([
    'underscore',
    'backbone',
    'models/login'
], function (_, Backbone, LoginModel) {
    'use strict';

    var LoginCollection = Backbone.Collection.extend({
        model: LoginModel
    });

    return LoginCollection;
});
